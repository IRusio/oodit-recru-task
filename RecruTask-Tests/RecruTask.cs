using RecruTask.Services;

namespace RecruTask_Tests;

public class RecruTask
{
    private readonly IRecruTaskService _recruTaskService;

    public RecruTask()
    {
        _recruTaskService = new RecruTaskService();
    }

    [Theory]
    [InlineData(new int[]{1,2,3,4,5,1,1}, new int[] {1})]
    [InlineData(new int[]{9,3,3,2,1,1,1,2,9,9,9}, new int[]{9,1})]
    [InlineData(new int[]{1,1,1,1,3,3,3,3,2,1,2,9,9,9}, new int[]{9,3,1})]
    [InlineData(new int[]{1,2,3,2,1,9,9,8}, new int[]{})]

    public void RecruTaskTestsFromExamples(int[] testInput, int[] expectedOutput)
    {
        var output = _recruTaskService.FindIfItemsInArrayRepeatAtLeastThreeTimes(testInput);
        
        Assert.Equal(expectedOutput, output);
    }
}