﻿using Microsoft.AspNetCore.Mvc;
using RecruTask.Services;

namespace RecruTask.Controllers;

public class RecruTaskController : Controller
{
    private readonly IRecruTaskService _recruTaskService;

    public RecruTaskController(IRecruTaskService recruTaskService)
    {
        _recruTaskService = recruTaskService;
    }

    public ActionResult Index()
    {
        return View();
    }

    [HttpPost]
    public ActionResult ProcessArray(string inputArray)
    {
        int[] parsedArray;

        try
        {
            parsedArray = PrepareArray(inputArray.Replace(" ", string.Empty));
        }
        catch (Exception e)
        {
            return View("Index", (inputArray, e.Message));
        }
        
        return View("Index", (inputArray, $"[{string.Join(',', _recruTaskService.FindIfItemsInArrayRepeatAtLeastThreeTimes(parsedArray.ToArray()))}]"));
    }

    
    //can be also moved to another class, but we don't have a lot of code there so no worth to refactor that.
    //Also integration tests would be nice there, to test more theory cases.
    private static int[] PrepareArray(string inputArray)
    {
        if (inputArray.FirstOrDefault() == '[' && inputArray.Last() == ']')
        {
            inputArray = inputArray.Substring(1, inputArray.Length - 2);
        }
        else if (inputArray.Length == 0)
        {
            return Array.Empty<int>();
        }
        else
        {
            //in production soft i would use more specified exceptions, and more specified Exception handling
            throw new Exception("not valid format. You should put array in []");
        }
        var result = inputArray.Split(',');
        var parsedResult = new List<int>();
        try
        {
            foreach (var element in result)
            {
                var parsed = int.Parse(element);
                parsedResult.Add(parsed);
            }
        }
        catch (Exception)
        {
            throw new Exception("error while processing");
        }


        return parsedResult.ToArray();
    }
}