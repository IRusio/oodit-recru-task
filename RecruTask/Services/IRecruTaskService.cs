﻿namespace RecruTask.Services;

public interface IRecruTaskService
{
    //can be a better name;
    public int[] FindIfItemsInArrayRepeatAtLeastThreeTimes(int[] input);
}