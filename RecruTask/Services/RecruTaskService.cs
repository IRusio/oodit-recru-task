﻿namespace RecruTask.Services;

public class RecruTaskService : IRecruTaskService
{
    public int[] FindIfItemsInArrayRepeatAtLeastThreeTimes(int[] input)
    {
        var numCounts = new Dictionary<int, int>();
        foreach (var num in input)
        {
            if (numCounts.ContainsKey(num))
            {
                numCounts[num]++;
            }
            else
            {
                numCounts[num] = 1;
            }
        }

        var descendingNums = numCounts.Where(kv => kv.Value >= 3)
            .OrderByDescending(kv => kv.Key)
            .Select(kv => kv.Key)
            .ToArray();

        return descendingNums;
    }
}